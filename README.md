# Lets Learn

### How to Run 
Run the application from IDE. 

**OR**

Run the application from terminal


For Mac `./gradlew run`

For Windows `gradlew.bat run`

### What to do

Follow the instructions from [Here](https://kiebot.notion.site/Lets-Learn-Project-Instructions-5884d31be4e74588817b1177d3f65551)